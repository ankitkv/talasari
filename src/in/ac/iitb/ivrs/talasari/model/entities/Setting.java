package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the settings database table.
 * 
 */
@Entity
@Table(name="settings")
@NamedQuery(name="Setting.findAll", query="SELECT s FROM Setting s")
public class Setting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="profile_id")
	private int profileId;

	private int favorites;

	@Column(name="max_call_duration")
	private int maxCallDuration;

	@Column(name="max_daily_single_user_calls")
	private int maxDailySingleUserCalls;

	@Column(name="max_listen_count")
	private int maxListenCount;

	@Column(name="max_recording_length")
	private int maxRecordingLength;

	@Column(name="playback_after_record")
	private int playbackAfterRecord;

	@Column(name="rating_after_listen")
	private int ratingAfterListen;

	public Setting() {
	}

	public int getProfileId() {
		return this.profileId;
	}

	public void setProfileId(int profileId) {
		this.profileId = profileId;
	}

	public int getFavorites() {
		return this.favorites;
	}

	public void setFavorites(int favorites) {
		this.favorites = favorites;
	}

	public int getMaxCallDuration() {
		return this.maxCallDuration;
	}

	public void setMaxCallDuration(int maxCallDuration) {
		this.maxCallDuration = maxCallDuration;
	}

	public int getMaxDailySingleUserCalls() {
		return this.maxDailySingleUserCalls;
	}

	public void setMaxDailySingleUserCalls(int maxDailySingleUserCalls) {
		this.maxDailySingleUserCalls = maxDailySingleUserCalls;
	}

	public int getMaxListenCount() {
		return this.maxListenCount;
	}

	public void setMaxListenCount(int maxListenCount) {
		this.maxListenCount = maxListenCount;
	}

	public int getMaxRecordingLength() {
		return this.maxRecordingLength;
	}

	public void setMaxRecordingLength(int maxRecordingLength) {
		this.maxRecordingLength = maxRecordingLength;
	}

	public int getPlaybackAfterRecord() {
		return this.playbackAfterRecord;
	}

	public void setPlaybackAfterRecord(int playbackAfterRecord) {
		this.playbackAfterRecord = playbackAfterRecord;
	}

	public int getRatingAfterListen() {
		return this.ratingAfterListen;
	}

	public void setRatingAfterListen(int ratingAfterListen) {
		this.ratingAfterListen = ratingAfterListen;
	}

}