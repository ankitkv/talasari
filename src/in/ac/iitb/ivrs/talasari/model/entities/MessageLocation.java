package in.ac.iitb.ivrs.talasari.model.entities;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the message_location database table.
 * 
 */
@Entity
@Table(name="message_location")
@NamedQuery(name="MessageLocation.findAll", query="SELECT m FROM MessageLocation m")
public class MessageLocation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="message_location_id")
	private int messageLocationId;

	private String location;

	//bi-directional many-to-one association to Message
	@ManyToOne
	@JoinColumn(name="message_id")
	private Message message;

	//uni-directional many-to-one association to Moderator
	@ManyToOne
	@JoinColumn(name="moderator_id")
	private Moderator moderator;

	public MessageLocation() {
	}

	public int getMessageLocationId() {
		return this.messageLocationId;
	}

	public void setMessageLocationId(int messageLocationId) {
		this.messageLocationId = messageLocationId;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Message getMessage() {
		return this.message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public Moderator getModerator() {
		return this.moderator;
	}

	public void setModerator(Moderator moderator) {
		this.moderator = moderator;
	}

}