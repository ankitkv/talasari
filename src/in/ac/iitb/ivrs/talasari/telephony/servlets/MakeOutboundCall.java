package in.ac.iitb.ivrs.talasari.telephony.servlets;

import in.ac.iitb.ivrs.talasari.config.Configs;
import in.ac.iitb.ivrs.telephony.base.util.IVRUtils;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MakeOutboundCall
 * This servlet is used to request an outbound call to be made to a particular user number. 
 */
@WebServlet("/MakeOutboundCall")
public class MakeOutboundCall extends HttpServlet {

	private static final long serialVersionUID = 1L;
       
    public MakeOutboundCall() {
        super();
    }

    /**
     * Request an outbound call to be made to the number specified in the HTTP parameter 'phone_no'.
     */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String phoneNo = request.getParameter("phone_no");
		log("Making outbound call");

		boolean status = IVRUtils.makeOutboundCall(phoneNo, Configs.Telephony.IVR_NUMBER, Configs.Telephony.APP_URL);
		if (status)
			log("Call succeeded");
		else
			log("Call failed");

		response.sendRedirect("index.jsp?status=" + status);
	}

}
